# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# This file is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. A copy of the
# License is located at
#
# http://aws.amazon.com/apache2.0/
#
# This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.

import boto3
import os
import datetime

# Create an S3 client
S3 = boto3.client('s3')

SourcePath = '/Users/dingqun/Desktop/logs/ad_sdk_logs'

'win_monitor.py'
BUCKET_NAME = 'mint-wh-mm'


def findPerfLogOfDay(dir, date_str):
    for home, dirs, files in os.walk(dir):
        for filename in files:
            print(filename)
            if (filename.find("ad-sdk." + date_str + '.ad-performance.log') != -1):
                return (filename, os.path.join(home, filename))
    return (None, None)


today = datetime.date.today()
formatted_today = today.strftime('%Y-%m-%d')
(SourceFileName, SourceFullPath) = findPerfLogOfDay (SourcePath, formatted_today)
if (SourceFileName != None):
    formatted_today = today.strftime('%Y/%m/%d/')
    TARGET_PATH = 'logs/sdk/aggr/perfs/' + formatted_today
    print('copy file from %s to %s' % (SourceFullPath, TARGET_PATH))
    result = S3.upload_file(SourceFullPath, BUCKET_NAME, TARGET_PATH + SourceFileName)
    print(result)
print("finished !!!")