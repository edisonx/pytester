#!/usr/bin/env python
# -*- coding:UTF-8 -*-
# 摇摇爆米花测试脚本
import sys
import time
import threading
import random

sys.path.append("libs")

SCREEN_ID = 41619   #屏幕ID 测试需要修改
USERS_NUMBER = 17   #创建进入游戏的玩家个数

global TESTER_INDEX
TESTER_INDEX = 1    #此处要大于0， 否则崩溃

#url = 'ws://gate0.edisonx.cn:2380'
#url = 'ws://h5.edisonx.cn:2380'
#url = 'ws://www.orchidstudio.cn:2360'
url = 'wss://miniapp.edisonx.cn/run:3360'

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
    import time

def on_message(ws, message):
    print('message:' + message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")



def getInitMsg():
    thumbImgs = (
        "http://touxiang.qqzhi.com/uploads/2012-11/1111004647841.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111014046331.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111021813342.png", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111112015966.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111032724256.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111105437504.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111122150831.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111122549416.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111020503617.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111111326568.jpg", \
        "http://www.qqzhi.com/uploadpic/2015-01-14/040518738.jpg")
    global TESTER_INDEX
    TESTER_INDEX += 1
    if(TESTER_INDEX >= len(thumbImgs)):
        TESTER_INDEX = 1
    return ("prof_c;" + str(SCREEN_ID) + ";小花生." + str(TESTER_INDEX) + \
            ";" + thumbImgs[TESTER_INDEX] + ";" + str(TESTER_INDEX)) + ";" + str(TESTER_INDEX)

def on_open(ws):

    def run(*args):
        time.sleep(1)
        m0 = getInitMsg()
        print m0
        ws.send(m0)
        time.sleep(1)
        ws.send("f")

        try:
            for j in range(1000):
                time.sleep(0.5)
                ws.send("bst:1")

        except Exception, e:
            print(e)
            return

        ws.close()
        print("thread terminating...")

    thread.start_new_thread(run, ())


def oneConnection():
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(url, on_message = on_message, on_error = on_error, on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

for i in range(USERS_NUMBER):
    print "user connecting NO.", i

    thrd = threading.Thread(target=oneConnection, args=())
    thrd.setDaemon(True)
    thrd.start()

    time.sleep(1)

time.sleep(3600*24)