#!/usr/bin/env python
# -*- coding:UTF-8 -*-
import sys
import time
import threading
import random

sys.path.append("libs")

SCREEN_ID = 6016
USERS_NUMBER = 30

global TESTER_INDEX
TESTER_INDEX = 1  #此处要大于0， 否则崩溃


url = 'wss://miniapp.edisonx.cn/flip:3345'

import websocket

try:
    import thread
except ImportError:
    import _thread as thread
    import time

def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")



def getInitMsg():
    thumbImgs = ("http://touxiang.qqzhi.com/uploads/2012-11/1111004647841.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111014046331.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111021813342.png", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111112015966.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111032724256.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111105437504.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111122150831.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111122549416.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111020503617.jpg", \
        "http://touxiang.qqzhi.com/uploads/2012-11/1111111326568.jpg", \
        "http://www.qqzhi.com/uploadpic/2015-01-14/040518738.jpg")
    global TESTER_INDEX
    TESTER_INDEX += 1
    if(TESTER_INDEX >= len(thumbImgs)):
        TESTER_INDEX = 1
    return ("prof_c;" + str(SCREEN_ID) + ";掌门狗" + str(TESTER_INDEX) + \
            ";" + thumbImgs[TESTER_INDEX-1] + ";" + str(TESTER_INDEX) + ";" + str(TESTER_INDEX) + ";中国;海淀;")

def on_open(ws):

    def run(*args):
        time.sleep(1)
        m0 = getInitMsg()
        print m0
        ws.send(m0)
        time.sleep(1)
        ws.send("f")

        try:
            x = 0
            y = 0
            random.seed()
            dx = random.randint(-8, 8) * 0.005
            dy = random.randint(-4, 4) * 0.005
            for j in range(10):
                for i in range(3):
                    time.sleep(2.5)

                    if x < -2 or x > 2:
                        dx = -dx
                        dy = random.randint(-40, 40) * 0.005
                    if y < -2 or y > 2:
                        dy = -dy
                        dx = random.randint(-20, 20) * 0.005
                    x += dx
                    y += dy
                    ws.send("mv:" + str(x) + "," + str(y))
                time.sleep(2)
                if j%5 == 4:
                    ws.send("u")

        except Exception, e:
            return

        ws.close()
        print("thread terminating...")

    thread.start_new_thread(run, ())


def oneConnection():
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(url, on_message = on_message, on_error = on_error, on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

for i in range(USERS_NUMBER):
    print "user connecting NO.", i

    thrd = threading.Thread(target=oneConnection, args=())
    thrd.setDaemon(True)
    thrd.start()

    time.sleep(1)

time.sleep(3600*24)