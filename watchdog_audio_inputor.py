#!usr/bin/env python
#coding=utf-8

import pyaudio
import wave
import time

LOG_PATH = 'c:\python27\edisonx\watchdog.log'

def playWave(wavPath):
    #define stream chunk
    chunk = 1024

    #open a wav format music
    f = wave.open(wavPath,"rb")
    #instantiate PyAudio
    p = pyaudio.PyAudio()
    #open stream
    stream = p.open(format = p.get_format_from_width(f.getsampwidth()),
                    channels = f.getnchannels(),
                    rate = f.getframerate(),
                    output = True)
    #read data
    data = f.readframes(chunk)

    #play stream
    while data:
        stream.write(data)
        data = f.readframes(chunk)

    #stop stream
    stream.stop_stream()
    stream.close()

    #close PyAudio
    p.terminate()

picNumber = 0#3
timeInterval = 30 * (picNumber + 1)

def log2file (msg):
    timeStamp = int(round(time.time() * 1000))

    fd = open(LOG_PATH, 'a')
    fd.write(str(timeStamp) + '[' + time.ctime() + ']' + msg + '\n')
    fd.close()
while True:
    log2file("start to play wav")
    playWave(r"d:/15s.wav")
    time.sleep(timeInterval)