#!/usr/bin/env python
# -*- coding:UTF-8 -*-
import json
import time
import threading

import httplib
import websocket

CONNECTION_NUMBER = 100

try:
    import thread
except ImportError:
    import _thread as thread
    import time

def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

# def getM0():
#     return ("prof_c;" + str(SCREEN_ID) + ";掌门狗" + str(TESTER_INDEX) +
#             ";http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIicWhBAYZ1lKRBk81HMicwSnQQHGqGsFjA4iabgiaib4NetticU21josSCibk5JU4WxHEmgMIKM0koWoAEQ/0;ocGKMwX-075nzPQgBOB98HaHPCN8;"
#             + str(TESTER_INDEX) + ";中国;海淀;1")

def on_open(ws):
    print "ws connected!"
    def run(*args):
        print "------------------------------------------"
        ws.send("box:fd")
        time.sleep(100)
        ws.close()

    thread.start_new_thread(run, ())




def webSocketConnect(ws_url):
    print "connecting " + ws_url
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(ws_url, on_message = on_message, on_error = on_error, on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

def httpGate():
    host = "www.91qzb.com"
    sub_url = '/thinkphp/public/index.php/api/Index/gate'

    conn = httplib.HTTPConnection(host)
    conn.request("GET", sub_url)
    r1 = conn.getresponse()
    # print r1.status, r1.reason
    data1 = r1.read()
    print data1

    result = json.loads(data1)

    ws_url = result['data']['url']

    webSocketConnect(ws_url)

for i in range(CONNECTION_NUMBER):
    print "user connecting NO.", i

    thrd = threading.Thread(target=httpGate, args=())
    thrd.setDaemon(True)
    thrd.start()

    time.sleep(5)

time.sleep(3600*24)