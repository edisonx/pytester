"""
Code that goes along with the Airflow tutorial located at:
https://github.com/apache/airflow/blob/master/airflow/example_dags/tutorial.py
"""

import os, shutil
from datetime import datetime, timedelta

base_path = "/Users/dingqun/WorkSpace/Android/mint-keyboard-docker-build"
sign_path = "/Users/dingqun/Documents/MI/signature"
mint_git_path = "/Users/dingqun/Documents/MI/mint_git"

def get_branch_command ():
    cmd = """
        cd %s
        br=`git branch | grep "*"`
        echo ${br/* /}
    """ % base_path
    return (os.popen(cmd).read())

def do_edit_build_gradle_version(branch_name, v7_or_v8):
    print("--------------------")
    ver_name = branch_name.split('v', 1)[1]
    ver_name = ver_name.split('_', 1)[0]
    print(ver_name)

    digit = '1'
    if (v7_or_v8 == 'v8'):
        digit = '2'
    elif (v7_or_v8 == 'v7'):
        digit = '1'
    else:
        raise RuntimeError('unkown v7 or v8 !!!!')
    ver_code = ver_name.replace(".", '') + digit
    print(ver_code)
    print("====================")
    # edite the version name and version code
    manifestXmlPath = base_path + '/ComicBook/app/build.gradle'
    f = open(manifestXmlPath, 'r')
    alllines = f.readlines()
    f.close()
    f = open(manifestXmlPath, 'w+')

    not_found = True
    for eachline in alllines:
        if (eachline.startswith("Integer VERSIONCODE = ")):
            new_line = "Integer VERSIONCODE = " + ver_code + "\n"
            f.writelines(new_line)
            not_found = False
        elif (eachline.startswith("String VERSIONNAME = \"")):
            new_line = "String VERSIONNAME = \"" + ver_name + "\"\n"
            f.writelines(new_line)
            not_found = False
        else:
            f.writelines(eachline)
    f.close()
    if (not_found):
        raise RuntimeError('version name not found !!!!')
    return ver_name

def build_apk_command ():
    cmd = """
        cd %s
        mkbuild.sh
    """ % base_path
    print (os.popen(cmd).read())

def search_file(path,name):

    for root, dirs, files in os.walk(path):  # path 为根目录
        for f in files:
            if (f.find(name) != -1):
                root = str(root)
                return os.path.join(root, f)
    return None

def move_mapping_apk(ver, v7_or_v8, channel):
    target_path = mint_git_path + '/' + ver

    need_create_folder = True
    if os.path.exists(target_path):
        print("%s found" % target_path)
        # if (v7_or_v8 == 'v8'):
        #     shutil.rmtree(target_path)
        #     print ("removing %s" % target_path)
        # else:
        need_create_folder = False

    if (need_create_folder):
        print("creating %s" % target_path)
        os.mkdir(target_path)

    target_path += '/' + channel
    if not os.path.exists(target_path):
        os.mkdir(target_path)
        print("creating %s" % target_path)
    else:
        if (v7_or_v8 == 'v8'):
            shutil.rmtree(target_path)
            print ("removing %s" % target_path)
            os.mkdir(target_path)
            print("creating %s" % target_path)

    target_path += '/' + v7_or_v8
    if not os.path.exists(target_path):
        os.mkdir(target_path)
        print("creating %s" % target_path)

    print('move mapping -> ' + target_path)
    shutil.move(sign_path + "/mapping", target_path)

    apk_path = search_file(sign_path ,'-' + ver + '-xhdpi-')

    if (apk_path == None):
        raise RuntimeError('No apk file !!!!')
    elif (apk_path.endswith ('a.apk.signed.apk') == -1):
        raise RuntimeError('No apk file !!!!')

    print('move signed apk -> ' + target_path)
    shutil.move(apk_path, target_path)

def edit_prebundle():
    print("========== editing build.gradle ==========")

    buildGradlePath = base_path + '/ComicBook/app/build.gradle'
    f = open(buildGradlePath, 'r')
    alllines = f.readlines()
    f.close()
    f = open(buildGradlePath, 'w+')

    counter = 0
    for eachline in alllines:
        if (eachline.find('buildConfigField "boolean", "PREBUNDLED", ') != -1):
            new_line = 'buildConfigField "boolean", "PREBUNDLED", "true"' + "\n"
            f.writelines(new_line)
            counter += 1
        else:
            f.writelines(eachline)
    f.close()
    if (counter != 2):
        raise RuntimeError('buildConfigField 2 place (%d) !!!!' % counter)

    print("========== editing AndroidManifest.xml ==========")

    manifestXmlPath = base_path + '/ComicBook/app/src/main/AndroidManifest.xml'
    f = open(manifestXmlPath, 'r')
    alllines = f.readlines()
    f.close()
    f = open(manifestXmlPath, 'w+')

    action_main_found = False
    action_launcher = False
    channel_prebundle = False
    for eachline in alllines:
        if (eachline.find('<action android:name="android.intent.action.MAIN') != -1):
            new_line = "<!-- " + eachline + "-->" + "\n"
            f.writelines(new_line)
            action_main_found = True
        elif (eachline.find('<category android:name="android.intent.category.LAUNCHER') != -1):
            new_line = "<!-- " + eachline + "-->" + "\n"
            f.writelines(new_line)
            action_launcher = True
        elif (eachline.find('android:value="PlayStore" />') != -1):
            new_line = 'android:value="PreBundled" />' + "\n"
            f.writelines(new_line)
            channel_prebundle = True
        else:
            f.writelines(eachline)
    f.close()

    print("========== editing install-referral.gradle ==========")

    install_ref = base_path + '/ComicBook/app/install-referral.gradle'
    f = open(install_ref, 'r')
    alllines = f.readlines()
    f.close()
    f = open(install_ref, 'w+')

    inst_ref = False
    for eachline in alllines:
        if (eachline.find('String REFERRAL =') != -1):
            new_line = 'String REFERRAL = XIAOMI_PREBUNDLE' + "\n"
            f.writelines(new_line)
            inst_ref = True
        else:
            f.writelines(eachline)
    f.close()

    if (not action_main_found):
        raise RuntimeError('android.intent.action.MAIN not found !!!!')

    if (not action_launcher):
        raise RuntimeError('category android:name="android.intent.category.LAUNCHER not found !!!!')

    if (not channel_prebundle):
        raise RuntimeError('DOWNLOAD_CHANNEL not found !!!!')

    if (not inst_ref):
        raise RuntimeError('Install referral not found !!!!')

def buildForChannel (channel):
    print("++++++++++ For %s ++++++++++" % channel)
    version_name = do_edit_build_gradle_version(branch_name, 'v8')
    print("++++++++++ Building Ver: %s ++++++++++" % version_name)
    build_apk_command()
    move_mapping_apk(version_name, 'v8', channel)

    version_name = do_edit_build_gradle_version(branch_name, 'v7')
    print("++++++++++ Building Ver: %s ++++++++++" % version_name)
    build_apk_command()
    move_mapping_apk(version_name, 'v7', channel)

############## MAIN ####################

BuildForGooglePlay = False
BuildForPreBundle = True

branch_name = get_branch_command()
print("++++++++++ Branch Name: %s ++++++++++" % branch_name)

if (BuildForGooglePlay):
    buildForChannel("GooglePlay")

if (BuildForPreBundle):
    edit_prebundle()
    buildForChannel("PreBundle")

